<?php

namespace Rkwebsolution\LaravelCartManager\Events;

class CartCreated
{
    /** @var array */
    public $cartData;

    public function __construct($cartData)
    {
        $this->cartData = $cartData;
    }
}
