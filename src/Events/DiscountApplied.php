<?php

namespace Rkwebsolution\LaravelCartManager\Events;

class DiscountApplied
{
    /** @var array */
    public $cartData;

    public function __construct($cartData)
    {
        $this->cartData = $cartData;
    }
}
