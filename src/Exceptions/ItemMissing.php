<?php

namespace Rkwebsolution\LaravelCartManager\Exceptions;

use OutOfBoundsException;

class ItemMissing extends OutOfBoundsException
{
}
