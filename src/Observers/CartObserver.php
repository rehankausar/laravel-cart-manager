<?php

namespace Rkwebsolution\LaravelCartManager\Observers;

use Rkwebsolution\LaravelCartManager\Models\Cart;

class CartObserver
{
    /**
     * Listen to the Cart deleting event.
     *
     * @param \Rkwebsolution\LaravelCartManager\Models\Cart $cart
     * @return void
     */
    public function deleting(Cart $cart)
    {
        $cart->items()->delete();
    }
}
